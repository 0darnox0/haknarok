import { Component, ViewEncapsulation, AfterViewChecked, ElementRef, ViewChild, OnInit, ContentChild, AfterViewInit } from '@angular/core';
import { Message } from './models/message';
import { MessageFormComponent } from './components/message-form/message-form.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements AfterViewInit {
  public message : Message;
  public messages : Message[];
  @ViewChild('container') private myScrollContainer: ElementRef;
  @ViewChild("messagelist") component1: MessageFormComponent;

  public ngAfterViewInit(): void
    {
      // this.component1.sendMessage();
      this.elRef.nativeElement.querySelector('.sug1').addEventListener('click', this.callFunc1.bind(this));
      this.elRef.nativeElement.querySelector('.sug2').addEventListener('click', this.callFunc2.bind(this));
    }

  callFunc1(event, msg) {
    this.component1.sendMessage("I want to report broken traffic lights");
  }

  callFunc2(event, msg) {
    this.component1.sendMessage("My neighbour uses coal in the chimney");
  }

  constructor(private elRef:ElementRef){

    this.message = new Message('', 'assets/images/user.png');
    this.messages = [
      new Message('Hey Emilie, what can I help you with?', 'assets/images/bot.png', new Date(), false),
      new Message('<div class="user-suggestion sug1">I want to report broken traffic lights</div>', 'assets/images/bot.png', new Date(), true),
      new Message('<div class="user-suggestion sug2">My neighbour uses coal in the chimney</div>', 'assets/images/bot.png', new Date(), true)
    ];
  }

  ngAfterViewChecked() {
        this.scrollToBottom();
        console.log(this.component1);
    }

    scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }
    }
}
