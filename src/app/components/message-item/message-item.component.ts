import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Message } from '../../models/message';

@Component({
  selector: 'message-item',
  templateUrl: './message-item.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./message-item.component.scss',]
})
export class MessageItemComponent implements OnInit {

  @Input('message')
  private message: Message;

  constructor() { }

  ngOnInit() {
  }

  public user() {
    return this.message.user;
  }

}
