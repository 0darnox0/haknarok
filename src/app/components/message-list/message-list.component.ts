import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Message } from '../../models/message';

@Component({
  selector: 'message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MessageListComponent implements OnInit {

  @Input('messages')
  private messages : Message[];

  constructor() { }

  ngOnInit() {
  }

}
