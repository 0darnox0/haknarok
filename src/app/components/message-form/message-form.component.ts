import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Message } from '../../models/message';
import { DialogflowService } from '../../services/dialogflow.service';
import * as RecordRTC from 'recordrtc';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'message-form',
  templateUrl: './message-form.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./message-form.component.scss',]
})
export class MessageFormComponent implements OnInit {

  //Lets initiate Record OBJ
  private record;
  //Will use this flag for detect recording
  private recording = false;
  //Url of Blob
  private url;
  private error;
  constructor(private domSanitizer: DomSanitizer,private dialogFlowService: DialogflowService) {
  }
  sanitize(url:string){
      return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
  /**
   * Start recording.
   */
  initiateRecording() {

      this.recording = true;
      let mediaConstraints = {
          video: false,
          audio: true
      };
      navigator.mediaDevices
          .getUserMedia(mediaConstraints)
          .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }
  /**
   * Will be called automatically.
   */
  successCallback(stream) {
      var options = {
          mimeType: "audio/wav",
          numberOfAudioChannels: 1
      };
      //Start Actuall Recording
      var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
      this.record = new StereoAudioRecorder(stream, options);
      this.record.record();
  }
  /**
   * Stop recording.
   */
  stopRecording() {
      this.recording = false;
      this.record.stop(this.processRecording.bind(this));
      this.sendAudio(this.record.blob);
      // var blob = this.record.blob;
      // console.log(blob);



  }
  /**
   * processRecording Do what ever you want with blob
   * @param  {any} blob Blog
   */
  processRecording(blob) {
      this.url = URL.createObjectURL(blob);
      var blob = this.record.blob;
      console.log(blob);

      var file = new File([blob], 'lecimymixtape.wav', {
        type: 'audio/wav'
    });

      var formData = new FormData();
     formData.append('file', file); // upload "File" object rather than a "Blob"
  }
  /**
   * Process Error.
   */
  errorCallback(error) {
      this.error = 'Can not play audio in your browser';
  }


  @Input('message')
  private message : Message;

  @Input('messages')
  private messages : Message[];

  //constructor(private dialogFlowService: DialogflowService) { }

  ngOnInit() {
  }



  public async sendAudio(audio: Blob) {
    this.message.timestamp = new Date();

    let response = await this.dialogFlowService.getResponseOnAudio(audio);
    console.log(response);


    //this.messages.push(this.message);
  }

  public async sendMessage(msg) {
    if (msg != undefined) {
      this.message.content = msg;
    }
    this.messages.push(this.message);

    // this.dialogFlowService.getResponse(this.message.content)
    //   .subscribe((lista: any) => {
    //     lista.result.fulfillment.messages.forEach((element) => {
    //       // this.resultados.push({ remetente: 'boot', mensagem: element.speech, data: lista.timestamp })
    //        this.messages.push(new Message(element.result.fulfillment.speech, 'assets/images/bot.png', element.timestamp))
    //     });
    //   })
    let response = await this.dialogFlowService.getResponse(this.message.content);
    console.log("Response", response);
    response.subscribe(res => {
      this.messages.push(
        new Message(res.result.fulfillment.speech, 'assets/images/bot.png', new Date(res.timestamp), false)
      );
    });

    this.message = new Message('', 'assets/images/user.png');
}


}
