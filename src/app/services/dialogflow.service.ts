import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { TextData } from '../models/textData';

@Injectable({
  providedIn: 'root'
})
export class DialogflowService {

  private baseURL: string = "http://localhost:8080";
  private token: string = environment.token;

  constructor(private http: HttpClient) { }


  public async getResponseOnAudio(audio: Blob) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    var ret: JSON = await this.http.post(`${this.baseURL}/audio`,  {audio: audio})
    .toPromise()
    .then(res => res as JSON);

    console.log(ret);

    return of({
        result: {
          fulfillment: {
            speech: ret['data'],//"Elo",
          },
        },
        timestamp: 32342344,
      });
  }

  public async getResponse(query: string){
    // let data = {
    //   query : query,
    //   lang: 'en',
    //   sessionId: '12345'
    // }
    //
    console.log('before posting' + JSON.stringify(query));
    var ret: JSON = await this.http.post(`${this.baseURL}/text`, {query: query})
    .toPromise()
    //.then(ret => console.log(ret));
    .then(res => res as JSON);

    //var test = JSON.stringify(ret);
    console.log(ret);

    return of({
        result: {
          fulfillment: {
            speech: ret['data'],//"Elo",
          },
        },
        timestamp: 32342344,
      });
  }

  public getHeaders(){
    let headers = new HttpHeaders();
    // headers.append('Authorization', `Bearer ${this.token}`);
    return headers;
  }

}
