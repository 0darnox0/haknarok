export class Message {
  content: string;
  timestamp: Date;
  avatar: string;
  public user: boolean ;

  constructor(content: string, avatar: string, timestamp?: Date, user= true ){
    this.content = content;
    this.timestamp = timestamp;
    this.avatar = avatar;
    this.user = user;
  }
}
