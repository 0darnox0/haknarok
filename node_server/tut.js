const dialogflow = require('dialogflow').v2beta1;
const uuid = require('uuid');
const util = require('util');
const fs = require('fs');
const Lame = require("node-lame").Lame;
const http = require("http");

const botId = 'newagent-44f0f';
const filePath = 'gorilla-in-mall.wav'
const sampleRate = '16000';



http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    var ret = sendAudio(botId, filePath, 'AUDIO_ENCODING_LINEAR_16', sampleRate);
    res.write(ret);
    res.end();
  }).listen(8080);



async function sendAudio(
    projectId,
    filename,
    encoding,
    sampleRateHertz
) {
    const sessionId = uuid.v4();

    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);

    const readFile = util.promisify(fs.readFile);
    const inputAudio = await readFile(filename);

    const request = {
        session: sessionPath,
        queryInput: {
            audioConfig: {
                audioEncoding: encoding,
                sampleRateHertz: sampleRateHertz,
                languageCode: 'en-US',
            },
        },
        inputAudio: inputAudio,
    };
    const responses = await sessionClient.detectIntent(request);

    console.log('Detected intent: ');
    const result = responses[0].queryResult;
    console.log(responses);
    console.log(`  Query: ${result.queryText}`);
    var ret_audio = responses[0].outputAudio;
    console.log(`  Response: ${result.fulfillmentText}`);
    if (result.intent) {
        console.log(`  Intent: ${result.intent.displayName}`);
    } else {
        console.log(`  No intent matched.`);
    }
    const editedAudio = {
        sampleRate: 16000,
        channelData: [
          ret_audio
        ]
      };

    fs.writeFile('./rawing.raw', ret_audio, 'binary', function(err) {
            if(err) console.log(err);
            else console.log("File saved");
    });
    return result.fulfillmentText;
}