const dialogflow = require('dialogflow').v2beta1;
const uuid = require('uuid');
const util = require('util');
const fs = require('fs');
const Lame = require("node-lame").Lame;
const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");

const botId = 'newagent-44f0f';
const filePath = 'gorilla-in-mall.wav'
const sampleRate = '16000';

const sessionId = uuid.v4();

const sessionClient = new dialogflow.SessionsClient();
const sessionPath = sessionClient.sessionPath(botId, sessionId);

var app = express()
app.use(bodyParser());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/', function(req, res) {
    sendText('I see scary gorilla', function(data) {  // notice that we pass in a function, but keep response handling up here
        res.send(data);  // HTTP responses must be strings
        
    });
});

app.post('/audio', function(req, res) {
    console.log(req);
    sendAudio(botId, filePath, 'AUDIO_ENCODING_LINEAR_16', sampleRate, function(data) {  // notice that we pass in a function, but keep response handling up here
        res.send(data);  // HTTP responses must be strings
        
    });
});

app.post('/text', function(req, res) {
    console.log(req.body['query']);
    sendText(req.body['query'], function(data) {  // notice that we pass in a function, but keep response handling up here
        res.send( {data: data});  // HTTP responses must be strings
        
    });
});

app.listen(8080);


/*http.createServer(function (req, res) {
    //var ret = sendAudio(botId, filePath, 'AUDIO_ENCODING_LINEAR_16', sampleRate);
    //console.log(ret);
    sendAudio(botId, filePath, 'AUDIO_ENCODING_LINEAR_16', sampleRate, function(data) {  // notice that we pass in a function, but keep response handling up here
        res.end(data);  // HTTP responses must be strings
    });
    //res.write(ret.fulfillmentText);
    //res.end();
  }).listen(8080);*/


async function sendText(textInput, cb) {

    const request = {
        session: sessionPath,
        queryInput: {
          text: {
            text: textInput,
            languageCode: 'en-us',
          },
        },
      };

    const responses = await sessionClient.detectIntent(request);

    return cb(responses[0].queryResult.fulfillmentText);
}


async function sendAudio(
    projectId,
    filename,
    encoding,
    sampleRateHertz,
    cb
) {


    const readFile = util.promisify(fs.readFile);
    const inputAudio = await readFile(filename);

    const request = {
        session: sessionPath,
        queryInput: {
            audioConfig: {
                audioEncoding: encoding,
                sampleRateHertz: sampleRateHertz,
                languageCode: 'en-US',
            },
        },
        inputAudio: inputAudio,
    };
    const responses = await sessionClient.detectIntent(request);

    //console.log('Detected intent: ');
    const result = responses[0].queryResult;
    //console.log(responses);
    //console.log(`  Query: ${result.queryText}`);
    var ret_audio = responses[0].outputAudio;
    //console.log(`  Response: ${result.fulfillmentText}`);
    /*if (result.intent) {
        console.log(`  Intent: ${result.intent.displayName}`);
    } else {
        console.log(`  No intent matched.`);
    } */
    const editedAudio = {
        sampleRate: 16000,
        channelData: [
          ret_audio
        ]
      };

    fs.writeFile('./rawing.raw', ret_audio, 'binary', function(err) {
            if(err) console.log(err);
            else console.log("File saved");
    });

    var retJson = JSON.stringify(
        {
            msg: result.fulfillmentText,
            audio: Buffer.from(ret_audio).toString('base64')
        }
    )
    return cb(retJson);
    //return cb(result.fulfillmentText);
}